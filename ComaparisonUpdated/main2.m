clear all;
clc;
close all;


%% Importing Data
Data = ImportDataFromExcel( 'phase_data_2.xlsx', 'Sheet1' );

ExperimentalData = []
ExperimentalData.vecF = Data.FrequencyKHz * 1e3; % [Hz]
ExperimentalData.vecA = Data.probeBeamAmplitudeAV;
ExperimentalData.vecA0 = Data.PumpBeamPowerA0W;
ExperimentalData.vecBeta = Data.Beta1;
ExperimentalData.vecPhi = Data.AlonSiPhaseDegree;

%% Simualtion

SimulationData = [];
SimulationData.vecF = ExperimentalData.vecF;
SimulationData.vecA = ExperimentalData.vecA;
SimulationData.vecA0 = ExperimentalData.vecA0;
SimulationData.vecBeta = ExperimentalData.vecBeta;
SimulationData.vecPhi = zeros( length(SimulationData.vecF), 1 );


const = []
const.d = 100e-9;
const.w0 = 30e-6; %[m] %TODO
const.RHO = 2.392e3;% Si  %Al 2.7e3 % [kg/m^3] density
const.c = 710;% Si %900 Al % [J/ (kg.K)] % specific heat Capacity

w1 = const.w0;

kParam = [];
kParam.nKmax = 10000 / sqrt( const.w0^2 + w1^2); %TODO
kParam.nKmin = 0;
kParam.nPointsofK = 1000;
%% Fitting
SIGMA_Rmin = 10;
SIGMA_Rmax = 148;
nPointsOfSIGMA_R = 50;
vecSIGMA_R = linspace( SIGMA_Rmin, SIGMA_Rmax, nPointsOfSIGMA_R );


SIGMA_Zmin = 10;
SIGMA_Zmax = 148;
nPointsOfSIGMA_Z = 50;
vecSIGMA_Z = linspace( SIGMA_Zmin, SIGMA_Zmax, nPointsOfSIGMA_Z );

optimizedSIGMA_R = 0;
optimizedSIGMA_Z = 0;
optimizedVecPhi = zeros( 1, length(SimulationData.vecF) );
minError = 1e6;

for i = 1 : nPointsOfSIGMA_R
    i*100 / nPointsOfSIGMA_R
    for j = 1 : nPointsOfSIGMA_Z
        const.SIGMA_R = vecSIGMA_R(i); % [W/K] % In plane thermal condcutivity % reference Section IV. Sample Results
        const.SIGMA_Z = vecSIGMA_Z(j); % Cross plane Thermal conductivity % reference Section IV. Sample Results
        
        SimulationData.vecPhi = getPhase( const, SimulationData, kParam   );
        tempError = sqrt(     sum(  ( real( SimulationData.vecPhi - ExperimentalData.vecPhi) ).^2 ) / length(SimulationData.vecPhi)     );
        if tempError < minError
            optimizedSIGMA_R = const.SIGMA_R;
            optimizedSIGMA_Z = const.SIGMA_Z;
            optimizedVecPhi = SimulationData.vecPhi;
            minError = tempError;
        end
    end
end
    




%% Plot 
semilogx(SimulationData.vecF, optimizedVecPhi, ExperimentalData.vecF, ExperimentalData.vecPhi , 'linewidth', 4 );
xlabel('Frequency(Hz)');
ylabel('Phase (deg)');
xlim([min(SimulationData.vecF), max(SimulationData.vecF)]);
legend('Fitted', 'Experiment')
SimulationData.vecF = SimulationData.vecF';
SimulationData.vecPhi = SimulationData.vecPhi';
%save('dataBabu.mat', 'vecF', 'vecPhi');
optimizedSIGMA_R
optimizedSIGMA_Z