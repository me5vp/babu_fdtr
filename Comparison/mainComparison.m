clear all;
clc;
close all;


%% Importing Data
Data = ImportDataFromExcel( 'phase_data.xlsx', 'Sheet1' );

ExperimentalData = []
ExperimentalData.vecF = Data.FrequencyKHz * 1e3; % [Hz]
ExperimentalData.vecA = Data.probeBeamAmplitudeAV;
ExperimentalData.vecA0 = Data.PumpBeamPowerA0W;
ExperimentalData.vecBeta = Data.Beta1;
ExperimentalData.vecPhi = Data.AlonSiPhaseDegree;

%% Simualtion

SimulationData = [];
SimulationData.vecF = ExperimentalData.vecF;
SimulationData.vecA = ExperimentalData.vecA;
SimulationData.vecA0 = ExperimentalData.vecA0;
SimulationData.vecBeta = ExperimentalData.vecBeta;
SimulationData.vecPhi = zeros( length(SimulationData.vecF), 1 );


const = []
const.d = 100e-9;
const.w0 = 6e-6; %[m]
const.RHO = 2.7e3;%2.392e3; % [kg/m^3] density
const.c = 900;%710; % [J/ (kg.K)] % specific heat Capacity

w1 = const.w0;

kParam = [];
kParam.nKmax = 100 / sqrt( const.w0^2 + w1^2);
kParam.nKmin = 0;
kParam.nPointsofK = 1000;
%% Fitting
SIGMA_Rmin = 8e3;
SIGMA_Rmax = 12e3;
nPointsOfSIGMA_R = 50;
vecSIGMA_R = linspace( SIGMA_Rmin, SIGMA_Rmax, nPointsOfSIGMA_R );


SIGMA_Zmin = 5e3;
SIGMA_Zmax = 7e3;
nPointsOfSIGMA_Z = 50;
vecSIGMA_Z = linspace( SIGMA_Zmin, SIGMA_Zmax, nPointsOfSIGMA_Z );

optimizedSIGMA_R = 0;
optimizedSIGMA_Z = 0;
optimizedVecPhi = zeros( 1, length(SimulationData.vecF) );
minError = 1e6;

for i = 1 : nPointsOfSIGMA_R
    i
    for j = 1 : nPointsOfSIGMA_Z
        const.SIGMA_R = vecSIGMA_R(i); % [W/K] % In plane thermal condcutivity % reference Section IV. Sample Results
        const.SIGMA_Z = vecSIGMA_Z(i); % Cross plane Thermal conductivity % reference Section IV. Sample Results
        
        SimulationData.vecPhi = getPhase( const, SimulationData, kParam   );
        tempError = sqrt(     sum(  ( real( SimulationData.vecPhi - ExperimentalData.vecPhi) ).^2 ) / length(SimulationData.vecPhi)     );
        if tempError < minError
            optimizedSIGMA_R = const.SIGMA_R;
            optimizedSIGMA_Z = const.SIGMA_Z;
            optimizedVecPhi = SimulationData.vecPhi;
            minError = tempError;
        end
    end
end
    




%% Plot 
semilogx(SimulationData.vecF, optimizedVecPhi, ExperimentalData.vecF, ExperimentalData.vecPhi , 'linewidth', 4 );
xlabel('Frequency(Hz)');
ylabel('Phase (deg)');
xlim([min(SimulationData.vecF), max(SimulationData.vecF)]);
legend('Fitted', 'Experiment')
SimulationData.vecF = SimulationData.vecF';
SimulationData.vecPhi = SimulationData.vecPhi';
%save('dataBabu.mat', 'vecF', 'vecPhi');
