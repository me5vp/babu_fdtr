function Phase = getPhase( const, SimulationData, kParam ) 




%% parameters
RHO = const.RHO ; % [kg/m^3] % density
c = const.c ; % [J / (kg. K)] % specific heat capacity
SIGMA_R = const.SIGMA_R; % [W / K] % In plane thermal condcutivity % reference Section IV. Sample Results
SIGMA_Z = const.SIGMA_Z; % [W/ K] % Cross plane Thermal conductivity % reference Section IV. Sample Results


w0 = const.w0; % [m]
w1 = w0;


d = const.d; % [m]

%% Sweeping variable

nFmin = min(SimulationData.vecF); % [Hz]
nFmax = max(SimulationData.vecF); % [Hz]
nPointsOfF = length(SimulationData.vecF);%
DEL_f = ( nFmax - nFmin ) / nPointsOfF;
%vecF = nFmin : DEL_f : nFmax; % [Hz]
vecOmega = 2*pi*SimulationData.vecF;

nKmax = kParam.nKmax;%100 / sqrt( w0^2 + w1^2);
nKmin = kParam.nKmin;%0
nPointsofK = kParam.nPointsofK;%500;
DEL_k = ( nKmax - nKmin ) / nPointsofK;
vecK = nKmin : DEL_k : nKmax;
vecK = vecK + DEL_k/2; % shifting K to the middle of each rectangle 

vecH_Omega = zeros( 1, nPointsOfF );
vecZ_omega = zeros( 1, nPointsOfF );
vecPhi = zeros(  nPointsOfF, 1 ); % [degree]

%% Solving No 8 Eqn

for i = 1 : length(vecOmega) % loop for every Frequency as Omega corresponds to each Freq
    BETA = SimulationData.vecBeta(i); %data from excel
    A = SimulationData.vecA(i); %data from excel
    A0 = SimulationData.vecA0(i); % data from excel
    nSumH_Omega = 0;
    for k = nKmin  : DEL_k : nKmax - DEL_k
        q = sqrt (     ( SIGMA_R * k^2 + RHO*c*1i*vecOmega(i) )  /  SIGMA_Z    );
        C = - SIGMA_Z * q * sinh( q*d );
        D = cosh( q * d );
        
        nSumH_Omega = nSumH_Omega   +   ( A0/(2*pi) ) * k * (-D/C) * exp(  -k^2 * (w0^2 + w1^2) / 8  )  * DEL_k;
        
    end
    vecH_Omega( i ) = nSumH_Omega; 
    vecZ_omega( i ) = BETA * vecH_Omega( i ); % solving eqn 9 for each Omega or Frequency
    vecPhi( i ) = (-1i * log ( vecZ_omega( i ) / A ))*(180/pi); % solving eqn 1 for Phi( in degree)
    
end

Phase = vecPhi;