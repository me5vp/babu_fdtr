clear all;
clc;
close all;


%% parameters
RHO = 2.392e3 ; % [kg/m^3] % density
c = 710 ; % [J / (kg. K)] % specific heat capacity
SIGMA_R = 10.6e3; % [W / K] % In plane thermal condcutivity % reference Section IV. Sample Results

SIGMA_Z = 6.5e3; % [W/ K] % Cross plane Thermal conductivity % reference Section IV. Sample Results


w0 = 25e-6; % [m]
w1 = w0;

BETA = 1;
A = 1;
A0 = 1;
d = 30e-9; % [m]

%% Sweeping variable

nFmin = 50e3; % [Hz]
nFmax = 20e6; % [Hz]
nPointsOfF = 500;
DEL_f = ( nFmax - nFmin ) / nPointsOfF;
vecF = nFmin : DEL_f : nFmax;
vecOmega = 2*pi*vecF;

nKmax = 2 / sqrt( w0^2 + w1^1);
nKmin = 0;
nPointsofK = 500;
DEL_k = ( nKmax - nKmin ) / nPointsofK;
vecK = nKmin : DEL_k : nKmax;
vecK = vecK + DEL_k/2; % shifting K to the middle of each rectangle 

vecH_Omega = zeros( 1, nPointsOfF );
vecZ_omega = zeros( 1, nPointsOfF );
vecPhi = zeros( 1, nPointsOfF );

%% Solving No 8 Eqn

for i = 1 : length(vecOmega) % loop for every Frequency as Omega corresponds to each Freq
    nSumH_Omega = 0;
    for k = nKmin  : DEL_k : nKmax - DEL_k
        q = sqrt (     ( SIGMA_R * k^2 + RHO*c*1i*vecOmega(i) )  /  SIGMA_Z    );
        C = - SIGMA_Z * q * sinh( q*d );
        D = cosh( q * d );
        
        nSumH_Omega = nSumH_Omega   +   ( A0/(2*pi) ) * k * (-D/C) * exp(  -k^2 * (w0^2 + w1^2) / 8  )  * DEL_k;
        
    end
    vecH_Omega( i ) = nSumH_Omega; 
    vecZ_omega( i ) = BETA * vecH_Omega( i ); % solving eqn 9 for each Omega or Frequency
    vecPhi( i ) = -1i * log ( vecZ_omega( i ) / A ); % solving eqn 1 for Phi
    
end
semilogx(vecF, vecPhi, 'linewidth', 4 );
xlabel('Frequency(Hz)');
ylabel('Phase (deg)');
xlim([nFmin, nFmax]);